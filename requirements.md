# Install instructions

## Jupyter Notebook

This chapter can be reproduced using a [conda environment](http://conda.pydata.org/docs/intro.html) with the
required dependencies installed. Dependencies are listed in `code/env_spec_XXX.txt`, where XXX is replaced by the OS platform.
Assuming you have the Anaconda Python distribution installed 
[see page](https://store.continuum.io/cshop/anaconda/) (make sure to run `conda update conda` for an up to date version), an environment can be recreated automatically by running the following command from the code folder:

> conda create -n reprojs --file env_spec_XXX.txt

Once that is completed, activate the environment by running:

> source activate reprojs

When you are done with the session, deactivate the environment with:

> source deactivate

### Developer note

The spec file can easily be recreated by running the following command on an
activated:

> conda list --export > env_spec_XXX.txt

## R dependencies

When using `R` the conda environment should be updated with rpy2 using the following command:
`conda install -c https://conda.anaconda.org/r rpy2`

Part of the analysis uses R through the [notebook interface](http://rpy.sourceforge.net/rpy2/doc-2.4/html/interactive.html#module-rpy2.ipython.rmagic). This requires a full working R setup in the machine as well.

[Add R dependencies here]
