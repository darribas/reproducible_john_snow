## Instructions for building the final paper

1. In Jupyter edit the notebook `code/docker/chapter.ipynb` and add text or code
   as needed
1. `make md` will build the markdown file from the notebook.
1. `make` will build a new markdown file if the notebook's changed and will generate a pdf file `chapter.pdf`
1. `make tex` will build a new markdown file if the notebook's changed and will convert  `chapter.tex` that we can then embed in the
   chapter template to get the preamble and styling correct.
