# Docker for chapter on reproducibility

Author: Serge Rey <sjsrey@gmail.com>

## Requirements

- Docker
- Docker toolbox on OS X https://www.docker.com/toolbox

## Builing the container

On Mac OS X open a Docker Quickstart Terminal



```
cd <path this repos lives on your host>
make build
```

This will take 20-30 minutes depending on the speed of your connection.

You only need do this once, however. After the image is built, you only
need to run and stop the container (see next).


## Running the container

```
make run
```

Find the ip for the container
```
$ uname -a
Linux markov 3.16.0-4-amd64 #1 SMP Debian 3.16.7-ckt20-1+deb8u3 (2016-01-17) x86_64 GNU/Linux
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED STATUS                    PORTS                    NAMES
cb4677c94adc        sjsrey/snow         "tini -- start-notebo"   4 minutes ago Up 4 minutes        0.0.0.0:8888->8888/tcp   compassionate_wing
```


open a browser at: http://0.0.0.0:8888


1. opening `R_snow.ipynb` will run an R kernel in jupyter
2. opening `chapter.ipynb` will run our chapter in a Python 2 kernel in
   jupyter


Note that the kernel has to be changed to Python 2 for the `chapter.ipynb`
notebook to render without errors.

## Shutting down the container

```
$ docker stop cb4677c94adc
```

You can then restart by `make run`




## Changing the Container

Note if you make any changes to the Docker file, you need to rerun `make build` before running the container again.



## To do


- trim the Dockerfile as there are likely packages not needed for the
  chapter. This works (and took a week to get working) so we can trim
from here
- install the histdate package via the Dockerfile rather than in the
  notebook
- clean up this directory as there are files we don't need
