#!/bin/bash

##  Update the package manager:

conda update --quiet --yes conda

# Create an environment loaded with the basic stack

conda create -n rjs -y --no-update-deps ipython jupyter pandas matplotlib pip seaborn scipy fiona=1.5 shapely=1.5.8 pyproj=1.9.4 six

# Activate environment

source activate rjs

# Install pip dependencies

pip install -U geopy descartes pysal
pip install --upgrade rpy2
pip install --no-deps geopandas

